﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginalState : MonoBehaviour {

    [HideInInspector] public Material originalMat;

    /// <summary>
    /// Resets the material to original material
    /// </summary>
    public void ResetState()
    {
        if (originalMat != null)
            gameObject.GetComponent<Renderer>().material = originalMat;
    }
}
