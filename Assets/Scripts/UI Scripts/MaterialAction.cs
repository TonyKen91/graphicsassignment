﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "", menuName = "Materials", order = 1)]
public class MaterialAction : Action
{

    public Color color;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public override void DoAction()
    {
        // This is used to change the colour of the control panels
        if (selectedObject != null && selectedObject.tag == "Controller Panel")
            selectedObject.GetComponent<Renderer>().material.SetColor("_Color", color);
    }

    public override void FinishAction()
    {

    }
}
