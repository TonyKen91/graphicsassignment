﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Selector : MonoBehaviour {

    public EventSystem eventSystem;

	// Update is called once per frame
	void Update () {
        // Checks if the pointer is over the UI 
		if (Input.GetMouseButtonDown(0) && eventSystem.IsPointerOverGameObject() == false)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                Action.selectedObject = hit.collider.gameObject;
            }
        }
	}
}
