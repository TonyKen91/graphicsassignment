﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "", menuName = "Skill", order = 1)]
public class SkillAction : Action
{

    public float cooldown = 0;
    public float duration = 0;

    public Material mat;
    private List<GameObject> bodyWithMaterial = new List<GameObject>();

    public bool extraCommand = false;


    /// <summary>
    /// This is used to do the action associated with the skill
    /// </summary>
    public override void DoAction()
    {
        selectedObject = FindObjectOfType<Player>().gameObject;

        // Checks for all the gameobjects under the player and turn their materials in the material corresponding to the skill
        Renderer[] bodyRenderers = selectedObject.GetComponentsInChildren<Renderer>();
        foreach(Renderer renderer in bodyRenderers)
        {
            GameObject body = renderer.gameObject;
            if (body.GetComponent<OriginalState>() == null)
            {
                OriginalState originState = body.AddComponent<OriginalState>();
                originState.originalMat = renderer.material;
            }
            renderer.material = mat;
        }

        // This gives the option having extra commands on top of this function's action
        if (extraCommand)
            selectedObject.GetComponent<ExtraCommand>().DoAction();
        activeAction = true;
    }


    /// <summary>
    /// Since skills have time limit therefore this is required to turn everything back default
    /// </summary>
    public override void FinishAction()
    {
        if (!activeAction)
            return;
        selectedObject = FindObjectOfType<Player>().gameObject;

        // Returns back all the materials to their original state
        OriginalState[] states = selectedObject.GetComponentsInChildren<OriginalState>();
        foreach (OriginalState state in states)
        {
            state.ResetState();
        }

        // This turns of the extra command after the duration of the skill
        if (extraCommand)
            selectedObject.GetComponent<ExtraCommand>().FinishAction();
        activeAction = false;
    }
}
