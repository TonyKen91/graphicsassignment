﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : ScriptableObject {

    public Sprite icon;
    public string desc;
    public static GameObject selectedObject;
    [HideInInspector] public bool activeAction = false;


    public abstract void DoAction();
    public abstract void FinishAction();
}
