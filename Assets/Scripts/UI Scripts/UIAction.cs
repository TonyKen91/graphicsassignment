﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAction : MonoBehaviour {

    public Action action;
    public Image icon;
    public Text nameTag;
    public Text descTag;
    public Button button;

    private float cooldownTimer;
    public float durationTimer;

    /// <summary>
    /// Sets the action's icon, name and description into the UI
    /// </summary>
    /// <param name="a"></param> the action argument
    public void SetAction (Action a)
    {
        action = a;
        if (icon)
            icon.sprite = action.icon;
        if (nameTag)
            nameTag.text = action.name;
        if (descTag)
            descTag.text = action.desc;
    }

    private void Start()
    {
        // hook up button
        button.onClick.AddListener(OnClicked);
    }

    public void OnClicked()
    {
        // Checks if the cooldown timer is finished before being able to turn the skill back on
        if (cooldownTimer <= 0)
        {
            action.DoAction();
            SkillAction skill = action as SkillAction;
            if (skill != null)
            {
                cooldownTimer = skill.cooldown;
                durationTimer = skill.duration;
            }
        }
    }

    private void Update()
    {
        // Reduce the cooldown timer 
        if (cooldownTimer > 0)
            cooldownTimer -= Time.deltaTime;

        if (durationTimer > 0)
        {
            durationTimer -= Time.deltaTime;
            if (durationTimer <= 0)
            {
                durationTimer = 0;
                action.FinishAction();
            }
        }
    }
}
