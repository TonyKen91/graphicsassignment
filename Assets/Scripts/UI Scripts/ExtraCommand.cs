﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraCommand : MonoBehaviour {

    public GameObject fireObject = null;
    public GameObject fireFootstep = null;

    private Footsteps steps = null;
    private GameObject defaultStep = null;

    private void Start()
    {
        steps = gameObject.GetComponent<Footsteps>();
    }

    /// <summary>
    /// This is used to do extra commands that the base class of skill action script can't handle
    /// </summary>
    public void DoAction()
    {
        if (fireObject != null)
            fireObject.SetActive(true);
        if (fireFootstep != null && steps != null)
        {
            if (steps.prefab != null)
                defaultStep = steps.prefab;
            steps.prefab = fireFootstep;
        }
    }

    /// <summary>
    /// Resets prefab and particles
    /// </summary>
    public void FinishAction()
    {
        if (fireObject != null)
            fireObject.SetActive(false);
        if (steps != null)
        {
            if (defaultStep != null)
                steps.prefab = defaultStep;

            else
                steps.prefab = null;
        }
    }

}
