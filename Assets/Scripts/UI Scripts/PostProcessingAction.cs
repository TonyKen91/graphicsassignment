﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "", menuName = "Post Processing Action", order = 1)]
public class PostProcessingAction : Action
{

    public Color color;
    public Material postProcessMat;
    private PostProcess cameraPostProcess;

    /// <summary>
    /// Just Sets the post processing material to the camera's post process script
    /// </summary>
    public override void DoAction()
    {
        cameraPostProcess = FindObjectOfType<PostProcess>();
        cameraPostProcess.mat = postProcessMat;
    }

    /// <summary>
    /// Post processing shaders don't have timer so this not necessary
    /// </summary>
    public override void FinishAction()
    {
        cameraPostProcess.mat = null;
    }
}
