﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIActionList : MonoBehaviour {

    public List<Action> actions = new List<Action>();
    public UIAction uiPrefab;

    Dictionary<Action, UIAction> uiActions = new Dictionary<Action, UIAction>();


	// Use this for initialization
	void Start () {
        uiPrefab.gameObject.SetActive(false);
        UpdateActions();
	}

    public void SetActions (Player player)
    {
        actions = player.actions;
        UpdateActions();
    }
	
	// Update is called once per frame
	void UpdateActions () {

        List<Action> garbageList = new List<Action>();

        // Added actions to the garbage list when it's not on the active list
        foreach (KeyValuePair<Action, UIAction> item in uiActions)
        {
            if (!actions.Contains(item.Key))
            {
                garbageList.Add(item.Key);
            }
        }

        // removes the UIActions associated to the ones in the garbage list
        foreach (Action garbage in garbageList)
        {
            Destroy(uiActions[garbage].gameObject);
            uiActions.Remove(garbage);
        }

        // Adds action that is not in the UIActions list
        foreach(Action action in actions)
        {
            if (!uiActions.ContainsKey(action))
            {
                UIAction instance = Instantiate(uiPrefab, transform);
                instance.SetAction(action);
                uiActions.Add(action, instance);
                instance.gameObject.SetActive(true);
            }

        }

    }
}
