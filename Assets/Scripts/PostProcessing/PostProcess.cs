﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostProcess : MonoBehaviour
{

    public Material mat;
    public Material defaultMaterial;

    // source texture and destination texture
    /// <summary>
    /// Apply post processing effects
    /// </summary>
    /// <param name="src"></param> Source Texture, this is the original input texture
    /// <param name="dest"></param> Destination Texture, this is where we output the result texture after applying the post processing effects
    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        // Uses default material when no post processing has been chosen
        if (mat == null)
        {
            mat = defaultMaterial;
        }

        ApplyProperty(src);

        // This is used specifically for blur to implement iteration with the blur shader
        if (mat.shader == Shader.Find("Custom/Blur"))
        {
            int blurIterations = mat.GetInt("_BlurIterations");

            // Creates two intermediary temporary render texture which can be used for iteration
            RenderTexture intermediate = RenderTexture.GetTemporary(src.descriptor);
            RenderTexture intermediate2 = RenderTexture.GetTemporary(src.descriptor);
            Graphics.Blit(src, intermediate2, mat);

            // Iterate through blur by using the temporary render texture to pass the previously blurred texture
            for (int i = 1; i < blurIterations - 1; i++)
            {
                intermediate = intermediate2;
                Graphics.Blit(intermediate, intermediate2, mat);
            }
            Graphics.Blit(intermediate2, dest, mat);

            // Release both the temporary render texture to make sure that you don't end up with memory leak
            intermediate.Release();
            intermediate2.Release();
        }
        // If not a blur just apply material directly
        else
            Graphics.Blit(src, dest, mat);

    }

    /// <summary>
    /// Sets property specific to certain post processing shader
    /// </summary>
    /// <param name="src"></param> Pass in the source texture to this function
    private void ApplyProperty(RenderTexture src)
    {
        // These sets the source width and height in the shader according to the source's width and height
        if (mat.HasProperty("_SourceWidth"))
        {
            mat.SetFloat("_SourceWidth", src.width);
        }

        if (mat.HasProperty("_SourceHeight"))
        {
            mat.SetFloat("_SourceHeight", src.height);
        }
    }

}
