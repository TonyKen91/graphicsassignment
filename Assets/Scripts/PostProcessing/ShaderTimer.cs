﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderTimer : MonoBehaviour {

    Renderer[] renderers;

	// Use this for initialization
	void Start () {
        renderers = GetComponentsInChildren<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		foreach (Renderer rend in renderers)
        {
            rend.material.SetFloat("_Timer", Time.time);
        }
	}
}
