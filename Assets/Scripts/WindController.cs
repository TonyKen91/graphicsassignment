﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindController : MonoBehaviour {

    public GameObject rainObject;
    public Slider windStrengthSlider;
    public Slider windDirectionSlider;
    public WindZone wind;

    private float sliderRainRatio;
    private float maxAngle = 60;

    // Sets the max stats of the wind
    private float sliderWindRatio;
    private float maxWindStrength = 10;
    private float maxWindTurbulence = 0.7f;
    private float maxPulseMag = 0.5f;
    private float maxPulseFreq = 5;

    // Use this for initialization
    void Start () {
        sliderRainRatio = maxAngle / windStrengthSlider.maxValue;
        UpdateEnvironment();
    }

    // Update is called once per frame
    public void UpdateEnvironment () {
        // Set the direction of the wind and rain
        rainObject.transform.rotation = Quaternion.Euler(new Vector3(-90 - windStrengthSlider.value * sliderRainRatio, windDirectionSlider.value, 0));
        wind.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, windDirectionSlider.value, 0));

        // Set wind properties depending on the strength of wind slider
        wind.windMain = windStrengthSlider.value * maxWindStrength / windStrengthSlider.maxValue;
        wind.windTurbulence = windStrengthSlider.value * maxWindTurbulence / windStrengthSlider.maxValue;
        wind.windPulseMagnitude = windStrengthSlider.value * maxPulseMag / windStrengthSlider.maxValue;
        wind.windPulseFrequency = windStrengthSlider.value * maxPulseFreq / windStrengthSlider.maxValue;
    }
}
