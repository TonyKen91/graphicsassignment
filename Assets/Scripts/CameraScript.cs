﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    private GameObject player;
    private GameObject cameraObject;

	// Use this for initialization
	void Start () {
        // Find and reference the player
        player = FindObjectOfType<Player>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        // Follows the position of the player and set this to that position
        transform.position = player.transform.position;

        // Right clicking gives the player freedom to look around their avatar
        if (Input.GetMouseButton(1))
        {
            float yRotation = Input.GetAxis("Mouse X");

            transform.Rotate(0, yRotation, 0);
        }
        // Releasing the right mouse button snaps the camera back to normal position
        else
        {
            transform.rotation = player.transform.rotation;
        }
    }
}
