﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainTransform : MonoBehaviour {

    private GameObject player;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        // Move Rain particle system to move with the player to reduce the amount of particle emission
        transform.position = player.transform.position + new Vector3 (0, 2.5f, 0);
	}
}
