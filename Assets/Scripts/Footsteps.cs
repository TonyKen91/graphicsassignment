﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour {


    public GameObject prefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Footstep()
    {
        // Instantiate particle system either dust or fire per character step
        if (prefab != null)
        {
            GameObject stepInstance = Instantiate(prefab);
            stepInstance.transform.position = transform.position;
            Destroy(stepInstance, 3);
        }
    }
}
