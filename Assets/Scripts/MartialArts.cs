﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MartialArts : MonoBehaviour {

    public Transform foot;
    public GameObject particles;

    Animator animator;

    public int comboIndex = 1;
    public float comboTimer = 2.0f;
    float resetTime;

    private void Hit()
    {
        GameObject blood = Instantiate(particles, foot);

    }


    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        // this makes the animator make up its own mind
        if (Input.GetKey(KeyCode.K))
        {
            // Sets the combo index on the animator
            animator.SetInteger("Kick", comboIndex);
            
            // Sets the time to reset the combo
            resetTime = Time.time + comboTimer;
        }
        else
            // resets the kick
            animator.SetInteger("Kick", 0);

        // resets combo after certain amount of time 
        if (Time.time > resetTime)
            comboIndex = 1;
    }
}
