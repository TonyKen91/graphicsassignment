﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RainController : MonoBehaviour {

    // 
    public Material skyboxMaterial;
    public Light directionalSunlight;
    public Slider rainSlider;
    public ParticleSystem rainParticle;
    [Range(0.3f, 0.8f)]
    public float maxSkyLight = 0.7f;
    [Range(0f, 0.3f)]
    public float minSkyLight = 0.2f;

    private float sliderLightRatio;
    private float sliderRainRatio;
    private float maxEmissionRate = 5000;
    private ParticleSystem.EmissionModule emission;

    // Use this for initialization
    void Start () {
        // Finding the ratio of the slider and the emission rate
        sliderRainRatio = maxEmissionRate / rainSlider.maxValue;
        UpdateEnvironment();
    }

    // Update is called once per frame
    public void UpdateEnvironment () {
        // Sets the emission's rate over time depending the amount of rain from the rain slider value
        emission = rainParticle.emission;
        emission.rateOverTime = rainSlider.value * sliderRainRatio;

        // Turns the light up or down depending on how heavy the rain is
        sliderLightRatio = (maxSkyLight - minSkyLight)/ rainSlider.maxValue;
        float visibility = maxSkyLight - rainSlider.value * sliderLightRatio;
        skyboxMaterial.SetColor("_Tint", new Color(visibility, visibility, visibility));

        // Turns the light up or down of the directional light as well
        visibility /= maxSkyLight;
        directionalSunlight.color = new Color(visibility, visibility, visibility);
    }
}
