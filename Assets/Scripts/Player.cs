﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {

    public List<Action> actions;

    // This solution of centralising changes in the player script might be too convoluted
    // It might be simpler to just pass a list of canvas stuff and set the actions in them
    // After all the action lists are public variables
    [System.Serializable]
    public class PlayerEvent : UnityEvent<Player>
    { }

    public PlayerEvent onActionsChanged;


    // Use this for initialization
    void Start()
    {

        // make a clone of each action so we can write to their data
        for (int i = 0; i < actions.Count; i++)
        {
            string nm = actions[i].name;
            actions[i] = Instantiate(actions[i]);
            actions[i].name = nm;
        }

        onActionsChanged.Invoke(this);
    }

    // Update is called once per frame
    void SetActions(List<Action> acs)
    {
        actions = acs;
        onActionsChanged.Invoke(this);

    }

    private void Update()
    {
        
    }
}
