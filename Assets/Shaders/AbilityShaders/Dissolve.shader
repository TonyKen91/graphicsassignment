﻿Shader "Custom/Dissolve"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_EdgeWidth ("Edge Width", Range(0.01, 1)) = 0.1
		_NoiseScale("Noise Scale", Range(0.01, 1)) = 0.1
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_NormalTex("Normal", 2D) = "bump" {}
	}

	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#include "noiseSimplex.cginc"

		#pragma surface surf Standard fullforwardshadows

		struct Input 
		{
		float2 uv_MainTex;
		float2 uv_BumpMap;
		float4 screenPos;
		float3 worldPos;
		};

	sampler2D _MainTex;
	sampler2D _NormalTex;
	half _Glossiness;  // this half means 16 bit number
	half _Metallic;
	fixed4 _EdgeColour;
	float _EdgeWidth;
	float _NoiseScale;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			// value in range 0 to 1
			float perlinNoise = (1.0 + snoise(10*IN.uv_MainTex))/2.0;

			clip(perlinNoise < (_SinTime.w) ? -1 : 1);
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			o.Smoothness = _Glossiness;
			o.Normal = UnpackNormal (tex2D(_NormalTex, IN.uv_BumpMap));
			o.Alpha = 1;
			o.Metallic = _Metallic;
		}
		
		ENDCG
	}
}