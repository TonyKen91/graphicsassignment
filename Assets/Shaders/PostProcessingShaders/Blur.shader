﻿Shader "Custom/Blur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BlurIterations("Blur Iterations", Range(1, 500)) = 1
		//_BlurSize("Blur Size", Range ( ))
		_SourceWidth("Source Width", int) = 1280
		_SourceHeight("Source Height", int) = 960
		_Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			int _BlurIterations;
			int _SourceWidth;
			int _SourceHeight;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
			float lum = (col.r + col.g + col.b);


			float2 texel;
			texel.x = 1.0f / _SourceWidth;
			texel.y = 1.0f / _SourceHeight;


			 //9-tap box kernel

			fixed4 colour = tex2D(_MainTex, i.uv);

			//for (int k = 0; k < _BlurIterations; k++)
			//{
				colour += tex2D(_MainTex, i.uv + texel * half2(-1, 1));
				colour += tex2D(_MainTex, i.uv + texel * half2(-1, 0));
				colour += tex2D(_MainTex, i.uv + texel * half2(-1, -1));
				colour += tex2D(_MainTex, i.uv + texel * half2(0, -1));
				colour += tex2D(_MainTex, i.uv + texel * half2(0, 1));
				colour += tex2D(_MainTex, i.uv + texel * half2(1, 1));
				colour += tex2D(_MainTex, i.uv + texel * half2(1, 0));
				colour += tex2D(_MainTex, i.uv + texel * half2(1, -1));
				colour /= 9;
			//}
			return colour;
			//return fixed4(lum * 0.439, lum * 0.259, lum * 0.078, 1);
			//return fixed4(lum * 0.5, lum * 1, lum * 1, 1);
			}
			ENDCG
		}
	}
}
